﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManageStudent
{
  
    public partial class Form1 : Form
    {
        public DataTable DB { get; set; }
        public Form1()
        {
            InitializeComponent();
            createDB();
            GetCBB(lopHp);
            lopHp.Items.Add("All");
            sortCbb.SelectedIndex = 0;
        }
        public void createDB()
        {
            DB = new DataTable();
            DB.Columns.AddRange(new DataColumn[]
            {
                 new DataColumn("MSSV", typeof(string)),
                 new DataColumn("Họ và Tên", typeof(string)),
                 new DataColumn("Giới Tính", typeof(bool)),
                 new DataColumn("Ngày Sinh", typeof(DateTime)),
                 new DataColumn("Địa Chỉ", typeof(string)),
                 new DataColumn("Phone", typeof(string)),
                 new DataColumn("Lớp HP", typeof(string))
            });
            //Tao 3 rows cho table DB
            DB.Rows.Add(new object[]
                {"102170244", "Ngô Trường Quốc", true, DateTime.Now, "Đà Nẵng", "0764483638", "17TCLC1" });
            DB.Rows.Add(new object[]
               {"102170236", "Diệp Chấn Khôi", true, DateTime.Now, "Bình Định", "0764323638", "17TCLC1" });
            DB.Rows.Add(new object[]
               {"1021702456", "Trần Phước Gia Thụy", true, DateTime.Now, "Đà Nẵng", "0324483632", "17TCLC1" });
            DB.Rows.Add(new object[]
               {"1021702426", "Ngô Tấn Trí", true, DateTime.Now, "Đà Nẵng", "0324323632", "17T3" });
        }
        public void GetCBB(ComboBox cb)
        {
            if(cb != null)
            {
                cb.Items.Clear();
            }
            cb.Items.AddRange( GetLopHP().Distinct().ToArray() );
        }
        public List<string> GetLopHP()
        {
            List<string> data = new List<string>();
            foreach(DataRow r in DB.Rows)
            {
                data.Add(r["Lớp HP"].ToString());
            }
            return data;
        }
        //get row reference to lopHP
        public DataTable GetSvByLopHP(string txtLopHp)
        {
            DataTable data = new DataTable();
            data = DB.Clone();
            if(txtLopHp == "All")
            {
                if (search_input.Text != "")
                {
                    try
                    {
                        foreach (DataRow r in DB.Rows)
                        {
                            if(string.Compare(r["Họ và Tên"].ToString(), search_input.Text) ==0)
                            {
                                data.Rows.Add(new object[]
                                    {
                                        r["MSSV"], r["Họ và Tên"], r["Giới Tính"],
                                        r["Ngày Sinh"], r["Địa Chỉ"], r["Phone"], r["Lớp HP"]
                                    });
                            }
                        }
                        return data;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    data = DB;
                }
            }
            else
            {
               if(search_input.Text == "")
                {
                    foreach (DataRow r in DB.Rows)
                    {
                        if (r["Lớp HP"].ToString() == txtLopHp)
                        {
                            data.Rows.Add(new object[]
                                {
                                r["MSSV"], r["Họ và Tên"], r["Giới Tính"],
                                r["Ngày Sinh"], r["Địa Chỉ"], r["Phone"], r["Lớp HP"]
                                });
                        }
                    }
                }
               else
                {
                    foreach (DataRow r in DB.Rows)
                    {
                        if (r["Lớp HP"].ToString() == txtLopHp && string.Compare(r["Họ và Tên"].ToString(), search_input.Text) ==0)
                        {
                            data.Rows.Add(new object[]
                                {
                                r["MSSV"], r["Họ và Tên"], r["Giới Tính"],
                                r["Ngày Sinh"], r["Địa Chỉ"], r["Phone"], r["Lớp HP"]
                                });
                        }
                    }
                }
            }
            return data;
        }

        public DataRow GetSVByName(string name)
        {
            DataRow data = DB.NewRow();
            try
            {
                foreach (DataRow item in DB.Rows)
                {
                    if(string.Compare(item["Họ và Tên"].ToString(), name) == 0)
                    {
                        data = item;
                        return data;
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataRow GetSVByMSSV(string mssv)
        {
            DataRow data = DB.NewRow();
            try
            {
                foreach (DataRow r in DB.Rows)
                {
                    if(r["MSSV"].ToString() == mssv)
                    {
                        data = r;
                        return data;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }
        }
        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewSelectedRowCollection data = dataGridView1.SelectedRows;
            try
            {
                if (data.Count == 1)
                {
                    string MSSVtxt = data[0].Cells["MSSV"].Value.ToString();
                    DataRow sv = GetSVByMSSV(MSSVtxt);
                    if(sv != null)
                    {
                        mssv.Text = sv["MSSV"].ToString();
                        mssv.ReadOnly = true;
                        name.Text = sv["Họ và Tên"].ToString();
                        phone.Text = sv["Phone"].ToString();
                        dateTimePicker1.Value = Convert.ToDateTime(sv["Ngày Sinh"]);
                        diachi.Text = sv["Địa Chỉ"].ToString();
                        if (Convert.ToBoolean(sv["Giới Tính"].ToString()))
                            Male.Checked = true;
                        else
                            Female.Checked = true;
                        GetCBB(comboBoxLopHp);
                        int index = comboBoxLopHp.Items.IndexOf(sv["Lớp HP"].ToString());
                        comboBoxLopHp.SelectedIndex = index;
                    }
                    else
                    {
                        MessageBox.Show("Invalid selected Item");
                    }

                }
                else
                {
                    Console.WriteLine("Invalid selected rows in table");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                throw;
            }
        }

        private void search_Click(object sender, EventArgs e)
        {
            try
            {
                if(lopHp.Text != "")
                {
                    dataGridView1.DataSource = GetSvByLopHP(lopHp.SelectedItem.ToString());
                }
                else
                {
                    MessageBox.Show("Vui long Chon lop hp");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
        public bool UpdateSV(params object[] data)
        {
            try
            {
                foreach (DataRow r in DB.Rows)
                {
                    if (r["MSSV"].ToString() == data[0].ToString())
                    {
                        r["Họ và Tên"] = data[1].ToString();
                        r["Lớp HP"] = data[2].ToString();
                        r["Phone"] = data[3].ToString();
                        r["Ngày Sinh"] = Convert.ToDateTime(data[4]);
                        r["Địa Chỉ"] = data[5].ToString();
                        r["Giới Tính"] = Convert.ToBoolean(data[6]);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void update_Click(object sender, EventArgs e)
        {
            string MSSVTxt = mssv.Text;
            string NameTxt = name.Text;
            string LopHPTxt = Convert.ToString(comboBoxLopHp.SelectedItem);
            string phoneTxt = phone.Text;
            DateTime birthdayTxt = Convert.ToDateTime(dateTimePicker1.Value);
            string DiaChiTxt = diachi.Text;
            bool MaleChecked = Male.Checked;
            bool FemaleChecked = Female.Checked;
            if (CheckFieldData(new object[] {
              MSSVTxt, NameTxt, LopHPTxt, phoneTxt, birthdayTxt, DiaChiTxt, MaleChecked, FemaleChecked
            }))
            {
                if(UpdateSV(MSSVTxt, NameTxt, LopHPTxt, phoneTxt, birthdayTxt, DiaChiTxt, MaleChecked, FemaleChecked))
                {
                    lopHp.SelectedItem = LopHPTxt;
                    dataGridView1.DataSource = GetSvByLopHP(LopHPTxt);
                }
            }
            else
                MessageBox.Show("Make sure you fill up all fields");
        }
        public bool CheckFieldData(object[] data)
        {
            if (data[0].ToString() == "" || data[1].ToString() == "" || data[2].ToString() == "" || data[3].ToString() == ""
                || data[4].ToString() == "" || data[5].ToString() == "" || (data[6].ToString()=="False"&&data[7].ToString()=="False")
                )
            {
                return false;
            }
            
            return true;
        }
        public bool Add(params object[] data)
        {
            try
            {
                DB.Rows.Add(new object[] 
                { 
                    data[0], data[1], data[2], data[3], data[4], data[5], data[6]
                });
                return true;
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message);
                return false;
            }
        }
        private void add_Click(object sender, EventArgs e)
        {
            string MSSVTxt = mssv.Text;
            string NameTxt = name.Text;
            string LopHPTxt = Convert.ToString(comboBoxLopHp.SelectedItem);
            string phoneTxt = phone.Text;
            DateTime birthdayTxt = Convert.ToDateTime(dateTimePicker1.Value);
            string DiaChiTxt = diachi.Text;
            bool MaleChecked = Male.Checked;
            bool FemaleChecked = Female.Checked;
            if(CheckFieldData(new object[] {
              MSSVTxt, NameTxt, LopHPTxt, phoneTxt, birthdayTxt, DiaChiTxt, MaleChecked, FemaleChecked
            }))
            {
                if(Add(MSSVTxt, NameTxt, MaleChecked, birthdayTxt, DiaChiTxt, phoneTxt, LopHPTxt))
                {
                    lopHp.SelectedIndex = 0;
                    dataGridView1.DataSource = GetSvByLopHP("All");
                }
            }
            else
            {
                MessageBox.Show("Make sure you fill up all the field");
            }
        }
        public bool DeleteSV(List<string> data)
        {
            try
            {
                foreach (string MSSV in data)
                {
                    foreach (DataRow item in DB.Select())
                    {
                        if (item["MSSV"].ToString() == MSSV)
                        {
                            DB.Rows.Remove(item);
                            break;
                        }
                    }
                    DB.AcceptChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void del_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection data = dataGridView1.SelectedRows;
            List <string> SV = new List<string>();
            try
            {
                if (data != null)
                {
                    foreach (DataGridViewRow r in data)
                    {
                        SV.Add(r.Cells["MSSV"].Value.ToString());
                    }
                    if(DeleteSV(SV))
                    {
                        string lop = lopHp.SelectedItem.ToString();
                        dataGridView1.DataSource = GetSvByLopHP(lop);
                    }
                    else
                    {
                        MessageBox.Show("Error");
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }
        private void sort_Click(object sender, EventArgs e)
        {
            List<string> listSv = new List<string>();
            try
            {
                foreach (DataRow item in DB.Rows)
                {
                    listSv.Add(item["Họ và Tên"].ToString());
                }
                listSv.Sort(delegate (string x, string y)
                {
                    if (string.Compare(x, y) == 0) return 0;
                    else if (string.Compare(x, y) < 0) return -1;
                    else return 1;
                });
                dataGridView1.DataSource = Sort(listSv);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
        public delegate bool MyCompare(object obj1, object obj2);
        public DataTable Sort(List<string> listSV)
        {
            DataTable data = new DataTable();
            data = DB.Clone ();
            DataTable Temp = new DataTable();
            Temp = DB.Copy();
            foreach (string SV in listSV)
            {
                foreach (DataRow r in Temp.Select())
                {
                    if(SV == r["Họ và Tên"].ToString())
                    {
                        data.Rows.Add(new object[]
                                   {
                                        r["MSSV"], r["Họ và Tên"], r["Giới Tính"],
                                        r["Ngày Sinh"], r["Địa Chỉ"], r["Phone"], r["Lớp HP"]
                                   });
                        Temp.Rows.Remove(r);
                    }
                }
                Temp.AcceptChanges();
            }
            return data;
        }
    }
}
